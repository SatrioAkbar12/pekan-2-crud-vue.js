<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Http\Resources\MahasiswaResource;

class MahasiswaController extends Controller
{
    public function index(){
        $mahasiswa = Mahasiswa::all();

        return MahasiswaResource::collection($mahasiswa);
    }
    public function store(Request $request){
        $mahasiswa = Mahasiswa::create([
            'nim' => $request->nim,
            'nama' => $request->nama
        ]);
    }
    public function show($id){
        $mahasiswa = Mahasiswa::find($id);

        return new MahasiswaResource($mahasiswa);
    }
    public function update($id, Request $request){
        Mahasiswa::where('id',$id)->update([
            'nama' => $request->nama
        ]);
    }
    public function delete($id){
        Mahasiswa::destroy($id);

        return "success";
    }
}
