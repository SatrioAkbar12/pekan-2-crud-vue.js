<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Mahasiswa</title>
    </head>
    <body>

        <h3>Database Mahasiswa</h3>

        <div id="app">

            <!-- Modal -->
            <div class="modal fade" id="modal-show" tabindex="-1" role="dialog" aria-labelledby="modal-show-title" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="modal-show-title">Detail Mahasiswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                            <div class="row">
                                <div class="col-4">
                                    NIM
                                </div>
                                <div class="col-8">
                                    : @{{ list_selected.nim }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    Nama
                                </div>
                                <div class="col-8">
                                    : @{{ list_selected.nama }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    Terdaftar pada
                                </div>
                                <div class="col-8">
                                    : @{{ list_selected.created_at }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    Diperbarui pada
                                </div>
                                <div class="col-8">
                                    : @{{ list_selected.updated_at }}
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>

            <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit-title" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="modal-edit-title">Detail Mahasiswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <form action="#">
                            <div class="form-group">
                                <label for="nim">NIM : </label>
                                <input type="text" id="nim" class="form-contol" name="nim" v-model="list_selected.nim" disabled>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama : </label>
                                <input type="text" id="nama" class="form-control" name="nama"  v-model="list_selected.nama">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" v-on:click="edit(list_selected)" data-dismiss="modal">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>

            <!-- Main -->
            <table class="table">
                <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(list, index) in list">
                        <td>@{{ list.nim }}</td>
                        <td>@{{ list.nama }}</td>
                        <td>
                            <button v-on:click="show(list)" class="btn btn-info" data-toggle="modal" data-target="#modal-show">Detail</button>
                            <button v-on:click="show(list)" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit">Edit</button>
                            <button v-on:click="hapus(index,list)" class="btn btn-danger">Hapus</button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div>
                <div class="fomr-group">
                    <input type="hidden" name="id" v-bind:value="id_terakhir"></div>
                </div>
                <div class="form-group">
                    <label for="nim">NIM :</label>
                    <input type="text" class="form-control" id="nim" name="nim" v-model="nim_baru">
                </div>
                <div class="form-group">
                    <label for="nama">Nama :</label>
                    <input type="text" class="form-control" id="nama" name="nama" v-model="nama_baru">
                </div>
                <div class="form-group">
                    <button v-on:click="create" class="btn btn-primary" >Simpan</button>
                </div>
            </div>


        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
        <script>
            let vo = new Vue ({
                el : "#app",
                data : {
                    nim_baru : '',
                    nama_baru : '',
                    list : [],
                    list_selected : [],
                    aaa : 'dsadhas',
                    bbb : 'djsaljdsakj'
                },
                methods : {
                    create: function(){
                        // POST /someUrl
                        this.$http.post('/api/mahasiswa/create', {nim: this.nim_baru, nama: this.nama_baru}).then(response => {
                            this.$http.get('/api/mahasiswa').then(response => {
                                let data = response.body.data
                                vo.list = data
                            })

                            this.nim_baru = ''
                            this.nama_baru = ''
                        })
                    },
                    show: function(list){
                        this.$http.get('/api/mahasiswa/'+list.id).then(response => {
                            let data = response.body.data
                            this.list_selected = data
                            console.log(this.list_selected)
                        })
                    },
                    edit: function(list){
                        this.$http.patch('/api/mahasiswa/'+list.id+'/edit', {nama: this.list_selected.nama}).then(response => {
                            this.$http.get('/api/mahasiswa').then(response => {
                                let data = response.body.data
                                vo.list = data
                            })
                        })
                    },
                    hapus: function(index,list){
                        this.$http.delete('/api/mahasiswa/'+list.id+'/delete').then(response => {
                            this.$http.get('/api/mahasiswa').then(response => {
                                let data = response.body.data
                                vo.list = data
                            })
                        })
                    }
                },
                computed : {
                    id_terakhir: function(){
                       return this.list.length
                    }
                },
                mounted : function(){
                    this.$http.get('/api/mahasiswa').then(response => {
                        let data = response.body.data
                        vo.list = data
                    })
                }
            })


        </script>
    </body>
</html>
